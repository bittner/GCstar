<?xml version="1.0" encoding="UTF-8"?>
<collection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="http://www.gcstar.org/schemas/gcm-revision4.xsd"
name="GCcoins">
    <lang>GCcoins</lang>
    <options>
        <defaults>
            <image>no.png</image>
            <groupby>country</groupby>
        </defaults>
        <fields>
            <cover>picture</cover>
            <id>gcsautoid</id>
            <play/>
            <title>name</title>
            <url>webPage</url>
            <search>
                <field>name</field>
                <field>country</field>
                <field>number1</field>
            </search>
            <results>
                <field>name</field>
                <field>country</field>
                <field>number1</field>
                <field>year</field>
                <field>series</field>
                <field>value</field>
                <field>mint</field>
                <field>metal</field>
            </results>
        </fields>
        <values id="favouriteYesno">
            <value displayed="">0</value>
            <value displayed="PanelFavourite">1</value>
        </values>
        <values id="types">
            <value displayed="Coin">coin</value>
            <value displayed="Banknote">banknote</value>
            <value displayed="Anniversary coin">an_coin</value>
            <value displayed="Token">token</value>
            <value displayed="Medal">medal</value>
            <value displayed="Non circulating coin">nc_coin</value>
        </values>
        <values id="axes">
            <value displayed="Medal">medal</value>
            <value displayed="Monetary">monetary</value>
            <value displayed="Turn">turn</value>
        </values>
        <values id="grades">
            <value displayed="Grade1">1</value>
            <value displayed="Grade2">2</value>
            <value displayed="Grade3">3</value>
            <value displayed="Grade4">4</value>
            <value displayed="Grade6">6</value>
            <value displayed="Grade8">8</value>
            <value displayed="Grade10">10</value>
            <value displayed="Grade12">12</value>
            <value displayed="Grade15">15</value>
            <value displayed="Grade20">20</value>
            <value displayed="Grade25">25</value>
            <value displayed="Grade30">30</value>
            <value displayed="Grade35">35</value>
            <value displayed="Grade40">40</value>
            <value displayed="Grade45">45</value>
            <value displayed="Grade50">50</value>
            <value displayed="Grade53">53</value>
            <value displayed="Grade55">55</value>
            <value displayed="Grade58">58</value>
            <value displayed="Grade60">60</value>
            <value displayed="Grade61">61</value>
            <value displayed="Grade62">62</value>
            <value displayed="Grade63">63</value>
            <value displayed="Grade64">64</value>
            <value displayed="Grade65">65</value>
            <value displayed="Grade66">66</value>
            <value displayed="Grade67">67</value>
            <value displayed="Grade68">68</value>
            <value displayed="Grade69">69</value>
            <value displayed="Grade70">70</value>
        </values>
    </options>

    <groups>
        <group id="Main" label="Main" />
        <group id="Description" label="Description" />
        <group id="Other" label="Other" />
    </groups>

    <random></random>

    <fields lending="false" tags="true">
        <field value="gcsautoid" 
            displayed="" 
            group="" 
            imported="false" 
            init="" 
            label="" 
            type="number" />
        <field value="name" 
            group="Main" 
            init="%value% %currency% %year% %city_letter% %series% " 
            label="Name" 
            type="formatted"
            imported="true" />
        <field value="picture"
            default="view" 
            group="Main" 
            init="" 
            label="Front" 
            type="image" 
            imported="true" />
        <field value="back" 
            default="view" 
            group="Main"
            init="" 
            label="Back" 
            type="image" 
            imported="true" />
        <field value="country" 
            group="Main" 
            label="Country" 
            type="history text" 
            init="France" 
            imported="true" />
        <field value="year" 
            group="Main" 
            init="" 
            label="Year" 
            type="number"
            imported="true" />
        <field value="calendar" 
            group="Main" 
            init="" 
            label="Calendar" 
            type="history text"
            imported="true" />
        <field value="quantity" 
            group="Main" 
            init="" 
            label="Quantity" 
            type="number"
            imported="true" />
        <field value="years_of_coinage" 
            group="Main" 
            init="" 
            label="Years of coinage" 
            type="history text"
            imported="true" />
        <field value="value" 
            group="Main" 
            init="" 
            label="Value" 
            type="number"
            imported="true" />
        <field value="currency" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Currency" 
            type="history text" 
            imported="true" />
        <field value="type" 
            group="Main" 
            init="coin"
            label="Type" 
            type="options" 
            values="types" 
            imported="true" />
        <field value="mint" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Mint" 
            type="history text" 
            imported="true" />
        <field value="mint_mark" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Mint mark" 
            type="history text" 
            imported="true" />
        <field value="mintmaster" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Mintmaster" 
            type="history text" 
            imported="true" />
        <field value="mintmaster_mark" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Mintmaster mark" 
            type="history text" 
            imported="true" />
        <field value="city" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="City" 
            type="history text" 
            imported="true" />
        <field value="city_letter" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="City letter" 
            type="history text" 
            imported="true" />
        <field value="series" 
            flat="true" 
            group="Main" 
            history="true" 
            init="" 
            label="Series" 
            type="history text" 
            imported="true" />
        <field value="metal" 
            flat="true"  
            group="Main" 
            history="true"
            init=""
            label="Metal" 
            type="single list" 
            imported="true" />
        <field value="weight" 
            group="Main" 
            init="" 
            label="Weight" 
            type="number"
            imported="true" />
        <field value="diameter" 
            group="Main" 
            init="" 
            label="Diameter" 
            type="number"
            imported="true" />
        <field value="depth" 
            group="Main" 
            init="" 
            label="Depth" 
            type="number"
            imported="true" />
        <field value="number" 
            group="Main" 
            init="1" 
            label="Number" 
            max="999" 
            min="1" 
            step="1" 
            type="number"
            imported="true" />
        <field value="form" 
            group="Main" 
            init="Ronde" 
            label="Form" 
            type="history text"
            imported="true" />
        <field value="axis" 
            group="Main" 
            init="monetary"
            label="Axis" 
            type="options" 
            values="axes"
            imported="true" />
        <field value="edge_type" 
            group="Main" 
            init="" 
            label="Edge type" 
            type="history text"
            imported="true" />
        <field value="edge1" 
            default="view" 
            group="Main" 
            init="" 
            label="Edge1" 
            type="image"
            imported="true" />
        <field value="edge2" 
            default="view" 
            group="Main" 
            init="" 
            label="Edge2" 
            type="image"
            imported="true" />
        <field value="edge3" 
            default="view" 
            group="Main" 
            init="" 
            label="Edge3" 
            type="image"
            imported="true" />
        <field value="edge4" 
            default="view" 
            group="Main" 
            init="" 
            label="Edge4" 
            type="image"
            imported="true" />
        <field value="condition" 
            group="Other" 
            init="" 
            label="Condition" 
            type="options" 
            values="grades"
            imported="false" />
        <field value="head" 
            group="Description" 
            init=""
            label="Head" 
            type="long text"
            imported="true" />
        <field value="tail" 
            group="Description" 
            init="" 
            label="Tail" 
            type="long text"
            imported="true" />
        <field value="edge" 
            group="Description" 
            init="" 
            label="Edge" 
            type="long text"
            imported="true" />
        <field value="comments" 
            group="Description" 
            init="" 
            label="Comments" 
            type="long text"
            imported="true" />
        <field value="buyed" 
            group="Other" 
            init="" 
            label="Buyed" 
            type="yesno"
            imported="false" />
        <field value="found" 
            group="Other" 
            init="" 
            label="Found" 
            type="yesno"
            imported="false" />
        <field value="bring" 
            group="Other" 
            init="" 
            label="Bring" 
            type="yesno"
            imported="false" />
        <field value="gift" 
            group="Other" 
            init="" 
            label="Gift" 
            type="yesno"
            imported="false" />
        <field value="location" 
            group="Other" 
            init="" 
            label="Location" 
            type="history text"
            imported="false" />
        <field value="added" 
            group="Other" 
            init="current" 
            label="PanelAdded" 
            type="date"
            imported="false" />
        <field value="catalogue1" 
            group="Other" 
            init="" 
            label="Catalogue1" 
            type="history text"
            imported="true" />
        <field value="number1" 
            group="Other" 
            init="" 
            label="Number1" 
            type="history text"
            imported="true" />
        <field value="estimate" 
            group="Other" 
            init="" 
            label="Estimate" 
            type="number"
            imported="true" />
        <field value="catalogue2" 
            group="Other" 
            init="" 
            label="Catalogue2" 
            type="history text"
            imported="false" />
        <field value="number2" 
            group="Other" 
            init="" 
            label="Number2" 
            type="history text"
            imported="false" />
        <field value="estimate2" 
            group="Other" 
            init="" 
            label="Estimate" 
            type="number"
            imported="false" />
        <field value="website" 
            group="Other" 
            init="" 
            label="Website" 
            type="url"
            imported="true" />
        <field value="history" 
            group="Other" 
            init="" 
            label="History" 
            type="long text"
            imported="true" />
        <field value="references" 
            group="Other" 
            init="" 
            label="References" 
            type="long text"
            imported="true" />
        <field value="webPage" 
            type="button" 
            format="url" 
            label="Url" 
            init="" 
            group="main" 
            imported="true"/>
        <field value="price" 
            group="Other" 
            init="" 
            label="Price" 
            type="number"
            imported="false" />
    </fields>

    <filters>
        <group label="Main">
            <filter field="type" comparison="contain" quick="true" />
        </group>
        <group label="Description">

        </group>
        <group label="Other">
            <filter field="catalogue1" comparison="contain" quick="true" />
            <filter field="catalogue2" comparison="contain" quick="true" />
        </group>
    </filters>

    <panels>
        <panel editable="true" label="PanelForm" name="form">
            <item type="line">
                <item type="value" for="name" expand="true" nomargin="true" />
                <item type="special" for="searchButton" nomargin="true" />
                <item type="special" for="refreshButton" nomargin="true" />
            </item>
            <item expand="true" type="notebook">
                <item expand="true" title="Main" type="tab" value="main">
                    <item type="line">
                        <item for="picture" height="150" type="value" width="180" />
                        <item for="back" height="150" type="value" width="180" />
                        <item cols="2" rows="2" type="table" >
                        <item row="0" col="0" for="country" type="label" />
                        <item row="0" col="1" for="country" type="value" />
                        <item row="1" col="0" for="type" type="label" />
                        <item row="1" col="1" for="type" type="value" />
                    </item>
                </item>
                    <item type="line">
                        <item cols="4" rows="10" type="table" >
                            <item row="1" col="0" for="value" type="label" />
                            <item row="1" col="1" for="value" type="value" expand="true" />
                            <item row="1" col="2" for="currency" type="label" />
                            <item row="1" col="3" for="currency" type="value" width="6" expand="true" />
                            <item row="2" col="0" for="year" type="label" width="5"  />
                            <item row="2" col="1" for="year" type="value" expand="true" />
                            <item row="2" col="2" for="years_of_coinage" type="label" />
                            <item row="2" col="3" for="years_of_coinage" type="value" expand="true" />
                            <item row="3" col="0" for="calendar" type="label" />
                            <item row="3" col="1" for="calendar" type="value" expand="true" />
                            <item row="3" col="2" for="quantity" type="label" />
                            <item row="3" col="3" for="quantity" type="value" expand="true" />
                            <item row="4" col="0" colspan="2" type="expander" title="Metal" collapsed="%metal%">
                                <item for="metal" type="value" />
                            </item>
                            <item row="4" col="2" for="diameter" type="label" />
                            <item row="4" col="3" for="diameter" type="value" expand="true" />
                            <item row="5" col="0" for="weight" type="label" width="5" />
                            <item row="5" col="1" for="weight" type="value" expand="true" />
                            <item row="5" col="2" for="depth" type="label" />
                            <item row="5" col="3" for="depth" type="value" expand="true" />
                            <item row="6" col="0" for="form" type="label" width="5" />
                            <item row="6" col="1" for="form" type="value" expand="true" />
                            <item row="6" col="2" for="axis" type="label" />
                            <item row="6" col="3" for="axis" type="value" expand="true" />
                            <item row="7" col="0" for="mint" type="label" />
                            <item row="7" col="1" for="mint" type="value" expand="true" />
                            <item row="7" col="2" for="mint_mark" type="label" />
                            <item row="7" col="3" for="mint_mark" type="value" expand="true" />
                            <item row="8" col="0" for="mintmaster" type="label" />
                            <item row="8" col="1" for="mintmaster" type="value" expand="true" />
                            <item row="8" col="2" for="mintmaster_mark" type="label" />
                            <item row="8" col="3" for="mintmaster_mark" type="value" expand="true" />
                            <item row="9" col="0" for="city" type="label" />
                            <item row="9" col="1" for="city" type="value" expand="true" />
                            <item row="9" col="2" for="city_letter" type="label" />
                            <item row="9" col="3" for="city_letter" type="value" expand="true" />
                        </item>
                    </item>
                    <item type="line">
                        <item rows="1" cols="4" type="table" expand="true" >
                            <item row="0" col="0" for="series" type="label" />
                            <item row="0" col="1" for="series" type="value" expand="true" />
                            <item row="0" col="2" for="number" type="label" />
                            <item row="0" col="3" for="number" type="value" width="4" />
                        </item>
                    </item>
                    <item type="line" >
                        <item for="edge_type" type="label" />
                        <item for="edge_type" type="value" expand="true" />
                    </item>
                    <item type="line" >
                        <item for="edge1" type="value" height="45" expand="true" />
                    </item>
                    <item type="line" >
                        <item for="edge2" type="value" height="45" expand="true" />  
                        <item for="edge3" type="value" height="45" expand="true" />
                        <item for="edge4" type="value" height="45" expand="true" />
                    </item>   
                </item>
                <item expand="true" title="Description" type="tab" value="description">
                    <item cols="2" rows="4" type="table">
                        <item col="0" row="0" for="head"     type="label" />
                        <item col="1" row="0" for="head"     type="value" expand="true" />
                        <item row="1" col="0" for="tail"     type="label" />
                        <item row="1" col="1" for="tail"     type="value" expand="true" />
                        <item row="2" col="0" for="edge"     type="label" />
                        <item row="2" col="1" for="edge"     type="value" expand="true" />
                        <item row="3" col="0" for="comments" type="label" />
                        <item row="3" col="1" for="comments" type="value" expand="true" />
                    </item>
                </item>
                <item expand="true" title="Other" type="tab" value="other">
                    <item type="line">
                        <item type="value" for="buyed" />
                        <item type="value" for="found" />
                        <item type="value" for="bring" />
                        <item type="value" for="gift" />
                    </item>
                    <item type="line" >
                        <item for="location" type="label" />
                        <item for="location" type="value" expand="true" />
                    </item>
                    <item type="line" >
                        <item for="added" type="label" />
                        <item for="added" type="value" />
                    </item>
                    <item type="line" >
                        <item for="condition" type="label" />
                        <item for="condition" type="value" />
                    </item>
                    <item type="line" >
                        <item for="price" type="label" />
                        <item for="price" type="value" />
                    </item>
                    <item type="line" >
                        <item for="website" type="label" />
                        <item for="website" type="value" expand="true" />
                    </item>
                    <item cols="4" rows="6" type="table" expand="true" >
                        <item row="0" col="0" for="catalogue1" type="label" />
                        <item row="0" col="1" for="catalogue1" type="value" expand="true" />
                        <item row="1" col="0" for="number1" type="label" />
                        <item row="1" col="1" for="number1" type="value" expand="true" />
                        <item row="2" col="0" for="estimate" type="label" />
                        <item row="2" col="1" for="estimate" type="value" />
                        <item row="0" col="2" for="catalogue2" type="label" />
                        <item row="0" col="3" for="catalogue2" type="value" expand="true" />
                        <item row="1" col="2" for="number2" type="label" />
                        <item row="1" col="3" for="number2" type="value" expand="true" />
                        <item row="2" col="2" for="estimate2" type="label" />
                        <item row="2" col="3" for="estimate2" type="value" />
                        <item row="4" col="0" for="history" type="label" />
                        <item row="4" col="1" for="history" type="value" colspan="3" expand="true" />
                        <item row="5" col="0" for="references" type="label" />
                        <item row="5" col="1" for="references" type="value" colspan="3" expand="true" />
                    </item>
                </item>
                <item type="tab" value="tagpanel" title="PanelTags">
                    <item type="line">
                        <item type="value" for="favourite" />
                    </item>
                    <item expand="true" for="tags" type="value" />
                </item>
            </item>
            <item type="line">
                <item type="value" for="webPage" expand="true" />
                <item for="deleteButton" type="special" expand="true" />
            </item>
        </panel>  
    </panels>
</collection>
