# GCstar
Management of collections (films, music, games, books, etc...)

(version française ci-dessous)

## Software

The application is written in Perl with Gtk modules : it runs on Linux, Windows and MacOS. 


## Project

The upstream project is [GCstar](http://www.gcstar.org/index.en.php) by Tian. The source was available under GPL licence on the [SVN repository](http://gna.org/projects/gcstar) (closed in May 2017). There is also a [discussion forum](http://forums.gcstar.org/).

GCstar uses a plugin system to gather information on collection items by scraping various web sites. The development of these plugins is currently very distributed : some appear in forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ), others in open source repositories.

This repository was created directly from the SVN repo using **git-svn**, trying to preserve tags and authors (some emails must be wrong though). The [master](https://gitlab.com/Kerenoc/GCstar/tree/master) branch was intended to be pulled directly from upstream "trunk" (**latest synchronisation 2017/05/15**). The [Test](https://gitlab.com/Kerenoc/GCstar/tree/Test) branch merges all current changes and should be used for environments with Gtk2. The [Gtk3](https://gitlab.com/Kerenoc/GCstar/tree/Gtk3) branch is the ungoing port to Gtk3 (mostly functional, need tweaking in the UI). As the original SVN repository is now unavailable, this GIT repository seems to be the best way to get the source code of GCstar.

GCstar can be used with 2 Android apps:

* **GCstar Viewer** to browse collections
* **GCstar Scanner** to add items to a collection by scanning their barcode

# GCstar
Gestionnaire de collection (films, musiques, livres, jeux, ...)

## Logiciel

GCstar est écrit en Perl avec des extension Gtk : il tourne sur Linux, Windows et MacOS.

## Projet

Le projet d'origine créé par Tian est [GCstar](http://www.gcstar.org/). Le code source en license GPL était disponible sur un [dépôt SVN](http://gna.org/projects/gcstar) (fermé en mai 2017). Il existe aussi un [forum de discussion](http://forums.gcstar.org/).

GCstar utilise un système de plugins pour collecter de l'information sur des sites web. Les développements de ces plugins sont actuellement très répartis et accessibles via des forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ) ou sur des forges.

Ce dépôt a été créé à partir de la source SVN en utilisant **git-svn**, en essayant de préserver les tags et les auteurs (sans doute des erreurs dans les adresses mail). La branche [master](https://gitlab.com/Kerenoc/GCstar/tree/master) était destinée à rester synchronisée avec le "trunk" de SVN (**dernière synchro 2017/05/15**). Les développements doivent se faire dans des branches spécifiques en attendant leur intégration dans un dépôt officiel. La branche [Test](https://gitlab.com/Kerenoc/GCstar/tree/Test) fusionne toutes les modifications et est la version à utiliser pour les environnements disposant de Gtk2.  La branche [Gtk3](https://gitlab.com/Kerenoc/GCstar/tree/Gtk3) héberge le portage en cours vers Gtk3 (version globalement fonctionnelle). Le dépôt SVN n'étant plus accessible (serveur fermé), ce dépôt GIT peut servir à accéder aux sources de GCstar. 

GCstar peut être utilisé avec deux applications Android :

* **GCstar Viewer** pour visualiser une collection
* **GCstar Scanner** pour entrer des éléments dans une collection à partir de leur code barre

# Installation

The newest versions of GCstar are not packaged and require a manual [installation](resources/Installation.md).

La dernière version de GCstar nécessite une [installation](resources/Installation.md) manuelle.

# Plugins

GCstar uses a plugin architecture to get information on items of collections from web sites. These plugins can easily be modified to adapt to changes of the web sites and can be updated without reinstalling GCstar.

A [list of plugins and their status](resources/Plugins.md) is used to keep track of changes.

GCstar est basé sur une architecture à base de modules complémentaires (plugin) permettant d'enrichir les informations à partir de sites web. Ces modules peuvent facilement être modifiés pour prendre en compte les évolutions des sites et peuvent être mis à jour sans réinstaller GCstar.

Une [listes des plugins](resources/Plugins.md) est utilisée pour tracer leur disponibilité et leurs évolutions.

# Contributions

Software contributions are welcome on this Gitlab repository or in the [GCstar forum](http://forums.gcstar.org/). Some help is also need to [check or contribute translations](resources/Translations.md) for already supported languages or new languages.

Les contributions au code de GCstar sont les bienvenus sur ce dépôt Gitlab ou sur le [forum GCstar](http://forums.gcstar.org/). De l'aide est aussi nécessaire pour [vérifier ou compléter les traductions](resources/Translations.md) pour les langues déjà supportée ou pour de nouvelles traductions.





