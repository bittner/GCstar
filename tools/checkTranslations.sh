#! bash
#
# testTranslation.sh
#
# script to test if the translations are complete
#
#

test -f gcstar/lib/gcstar/GCLang/EN/GCstar.pm || (echo === Execute at the root of the repository && exit)

cd gcstar/lib/gcstar/GCLang

getFieldsList() {
    grep "=>" $1 | sed 's/ *=>.*//' | sed "s/'//g" | sed 's/^[ \t]*//' | sort
}

for lang in *
do
    test -d $lang &&
	(
	cd EN
    	echo "======== Checking missing translations for $lang"
    	find . -name "*.pm" -type f -print | \
    	    while read i
    	    do
    		getFieldsList $i > tmp_EN.txt
    		getFieldsList ../$lang/$i > tmp_$lang.txt
    		cmp tmp_EN.txt tmp_$lang.txt 2> /dev/null > /dev/null || (
    		    echo "         ------ $i"
    		    diff tmp_EN.txt tmp_$lang.txt | grep "< " | sed 's/^< //' | grep -v "< _lowercase_" )
    	    done
	cd ..
	)
done
