#! bash
#
# checkLang.sh
#
# script to test if the translations are complete using checkLang.pl
#
#
export PATH="../Perl/bin/:$PATH"

test -f gcstar/lib/gcstar/GCLang/EN/GCstar.pm
if [ $? -ne 0 ]; then
    echo === Execute at the root of the repository
	exit
fi

(
	cd gcstar/lib/gcstar/GCLang
	for lang in *
	do
		test -d $lang && echo $lang
	done | grep -v EN
) | \
while read lang
do
	echo === Checking translation for $lang
	perl tools/checkLang.pl $lang > tools/Translations_Checks/checkLang_$lang.txt
done	

